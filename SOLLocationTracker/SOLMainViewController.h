//
//  SOLMainView.h
//  SOLLocationTracker
//
//  Created by quang.tran on 6/15/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface SOLMainViewController : UITableViewController <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (strong, nonatomic) CLLocation *desiredLocation;

@property (weak, nonatomic) IBOutlet UITextField *txtCurrentLongtitude;
@property (weak, nonatomic) IBOutlet UITextField *txtCurrentLatitude;

@property (weak, nonatomic) IBOutlet UITextField *txtDesiredLongtitude;
@property (weak, nonatomic) IBOutlet UITextField *txtDesireLatitude;

@property (weak, nonatomic) IBOutlet UIButton *btnShowDesiredLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentReadableAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDesiredReadableAddress;

@end
