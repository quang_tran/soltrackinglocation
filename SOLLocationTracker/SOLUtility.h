//
//  SOLUtility.h
//  SOLLocationTracker
//
//  Created by quang.tran on 6/16/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface SOLUtility : NSObject

+ (float)getDistanceBetweenLocation:(CLLocation*)locationA andLocationB:(CLLocation*)locationB;

@end
