//
//  ViewController.m
//  SOLLocationTracker
//
//  Created by quang.tran on 6/15/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//

#import "SOLMapViewController.h"
#import "SOLUtility.h"

#define METERS_PER_MILE 1609.344

@interface SOLMapViewController () {
    CLLocation *touchedLocation;
    UILongPressGestureRecognizer *lpgr;
}

@end

@implementation SOLMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleGesture:)];
    lpgr.minimumPressDuration = 1.0;  //user must press for 1 seconds
    [self.mapView addGestureRecognizer:lpgr];
    
    //add current location
    _mapView.showsUserLocation = YES;
    if (self.currentLocation) {
        CLLocationCoordinate2D coord = self.currentLocation.coordinate;
        CLLocationCoordinate2D zoomLocation;
        zoomLocation.latitude = 39.281516;
        zoomLocation.longitude= -76.580806;
        
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coord, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
        
        [_mapView setRegion:viewRegion animated:YES];
    }
    
    //add desired location
    if (self.desiredLocation) {
        [self createDesiredLocationPinFromCor:self.desiredLocation.coordinate];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //clean the map
    for (id annotation in self.mapView.annotations) {
        [self.mapView removeAnnotation:annotation];
    }
    
    //remove gesture
    [self.view removeGestureRecognizer:lpgr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpers
- (void)createDesiredLocationPinFromCor:(CLLocationCoordinate2D)myCoordinate {
    //Create your annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    // Set your annotation to point at your coordinate
    point.coordinate = myCoordinate;
    point.title = @"My Desired Location";
    point.subtitle = @"This is where i want to be, this is where i belong!";
    //Drop pin on map
    [self.mapView addAnnotation:point];
    NSLog(@"Did set desired location");
}

#pragma mark - Touch Action
- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    //clean the map
    for (id annotation in self.mapView.annotations) {
        [self.mapView removeAnnotation:annotation];
    }
    
    //add pin
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D location =
    [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    touchedLocation = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];
    self.lblLongitude.text = [NSString stringWithFormat:@"%f (long)", location.longitude];
    self.lblLatitude.text = [NSString stringWithFormat:@"%f (lati)", location.latitude];
    
    [self createDesiredLocationPinFromCor:location];
    
    //calculate distance
    CLLocation *tempLocation = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];
    self.lblDistance.text = [NSString stringWithFormat:@"%.2f (meters)",[SOLUtility getDistanceBetweenLocation:self.currentLocation andLocationB:tempLocation]];
    
    //get readable name
    CLGeocoder*geocoder = [[CLGeocoder alloc] init];
    self.lblReadableAddress.text = @"Mapping....";
    [geocoder reverseGeocodeLocation:tempLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            CLPlacemark *placemark = [placemarks lastObject];
            
            self.lblReadableAddress.text = [NSString stringWithFormat:@"%@ %@, %@ %@, %@",
                                placemark.thoroughfare.length > 0 ? placemark.thoroughfare : @"",
                                placemark.subThoroughfare.length > 0 ? placemark.subThoroughfare : @"",
                                placemark.postalCode.length > 0 ? placemark.postalCode : @"",
                                placemark.locality.length > 0 ? placemark.locality : @"",
                                placemark.country.length > 0 ? placemark.country : @""];
            
        } else {
             self.lblReadableAddress.text = @"Can't mapping!";
        }
        
    }];

}

#pragma mark - Action

- (IBAction)setdesiredLocationAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    if (self.onSetDesiredLocation) {
        self.onSetDesiredLocation(touchedLocation);
    }
}



@end
