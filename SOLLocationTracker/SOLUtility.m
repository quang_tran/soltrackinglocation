//
//  SOLUtility.m
//  SOLLocationTracker
//
//  Created by quang.tran on 6/16/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//

#import "SOLUtility.h"

@implementation SOLUtility

+ (float)getDistanceBetweenLocation:(CLLocation *)locationA andLocationB:(CLLocation *)locationB {
    return [locationA distanceFromLocation:locationB];
}


@end
