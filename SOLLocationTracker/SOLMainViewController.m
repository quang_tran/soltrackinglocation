//
//  SOLMainView.m
//  SOLLocationTracker
//
//  Created by quang.tran on 6/15/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//

#import "SOLMainViewController.h"
#import "SOLMapViewController.h"

@interface SOLMainViewController () {
    float distance;
    CLGeocoder *geocoder;
}

@end

@implementation SOLMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //just start the location tracking for older iOS
    [self startStandardUpdates];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.desiredLocation) {
        [self.btnShowDesiredLocation setTitle:@"Show on map" forState:UIControlStateNormal];
        _txtDesiredLongtitude.text = [NSString stringWithFormat:@"%f", self.desiredLocation.coordinate.longitude];
        _txtDesireLatitude.text = [NSString stringWithFormat:@"%f", self.desiredLocation.coordinate.latitude];
        [self fillDesiredLocationData];
    } else {
        [self.btnShowDesiredLocation setTitle:@"Find a desired location" forState:UIControlStateNormal];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - CLLocationManagerDelegate

- (void)startStandardUpdates
{
    // Create the location manager if this object does not
    // already have one.
    if (nil == self.locationManager)
        self.locationManager = [[CLLocationManager alloc] init];
    
    // Code to check if the app can respond to the new selector found in iOS 8. If so, request it.
    if([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    // Set a movement threshold for new events.
    self.locationManager.distanceFilter = 100; // meters
    
    [self.locationManager startUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = locations.lastObject;
    _currentLocation = location;
    
    //update view
    _txtCurrentLongtitude.text = [NSString stringWithFormat:@"%f", _currentLocation.coordinate.longitude];
    _txtCurrentLatitude.text = [NSString stringWithFormat:@"%f", _currentLocation.coordinate.latitude];
    
    //update location readable address
    [self fillCurrentLocationData];
    
    //check if current location and desire location is near (meters)
    distance = [self getDistance];
    NSLog(@"Distance: %f", distance);
    if (self.desiredLocation != nil && distance <= 100) {
        [self showAlert];
        [self fireLocalNotification];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:[error description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

#pragma mark - Helpers
- (float)getDistance {
    return [self.currentLocation distanceFromLocation:self.desiredLocation];
}
- (void)showAlert {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:[NSString stringWithFormat:@"You are getting near to your desired location: %.2f away",[self getDistance]] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
}

- (void)fireLocalNotification {
    // Schedule the notification
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.alertBody = [NSString stringWithFormat:@"You are getting near to your desired location: %.2f away",[self getDistance]];
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    [[UIApplication sharedApplication] scheduleLocalNotification: localNotification];
}

- (void)fillDesiredLocationData {
    self.lblDesiredReadableAddress.text = @"Mapping....";
    if (geocoder == nil) {
        geocoder = [[CLGeocoder alloc] init];
    }
    [geocoder reverseGeocodeLocation:self.desiredLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            CLPlacemark *placemark = [placemarks lastObject];
            
            self.lblDesiredReadableAddress.text = [NSString stringWithFormat:@"%@ %@, %@ %@, %@",
                                placemark.thoroughfare.length > 0 ? placemark.thoroughfare : @"",
                                placemark.subThoroughfare.length > 0 ? placemark.subThoroughfare : @"",
                                placemark.postalCode.length > 0 ? placemark.postalCode : @"",
                                placemark.locality.length > 0 ? placemark.locality : @"",
                                placemark.country.length > 0 ? placemark.country : @""];
            
        } else {
            self.lblDesiredReadableAddress.text = @"Can't mapping!";
        }
        
    }];
}

- (void)fillCurrentLocationData {
    if (geocoder == nil) {
        geocoder = [[CLGeocoder alloc] init];
    }
    self.lblCurrentReadableAddress.text = @"Mapping....";
    [geocoder reverseGeocodeLocation:self.currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            CLPlacemark *placemark = [placemarks lastObject];
            
            self.lblCurrentReadableAddress.text = [NSString stringWithFormat:@"%@ %@, %@ %@, %@",
                                                   placemark.thoroughfare.length > 0 ? placemark.thoroughfare : @"",
                                                   placemark.subThoroughfare.length > 0 ? placemark.subThoroughfare : @"",
                                                   placemark.postalCode.length > 0 ? placemark.postalCode : @"",
                                                   placemark.locality.length > 0 ? placemark.locality : @"",
                                                   placemark.country.length > 0 ? placemark.country : @""];
            
        } else {
            self.lblCurrentReadableAddress.text = @"Can't mapping!";
        }
        
    }];

}

#pragma mark - Action
- (IBAction)checkDistanceAction:(id)sender {
    NSString *message = [NSString stringWithFormat:@"There is %f between your current location and desired location", [self getDistance]];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
}


- (IBAction)showMapForCurrentLocation:(id)sender {
    if (self.currentLocation) {
        [self performSegueWithIdentifier:@"showMap" sender:self];
    }
}

- (IBAction)showMapForDesiredLocation:(id)sender {
    [self performSegueWithIdentifier:@"showMap" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    SOLMapViewController *mapViewController = [segue destinationViewController];

        mapViewController.currentLocation = self.currentLocation;

    
  
        mapViewController.desiredLocation= self.desiredLocation;
    
    //call back here when set desire location is tapped
    mapViewController.onSetDesiredLocation = ^(CLLocation *sender)
    {
        self.desiredLocation = sender;
    };
}




@end
